---
title: "Encodings"
weight: 90
---

{{% notice warning %}}
This wiki is deprecated. Visit https://ddnet.org/docs/libtw2/ for the current version of this page
{{% /notice %}}

<!-- This is an auto generated file. If you want to make changes edit scripts/import-twmap2-doc.sh instead -->

{{% notice note %}}
This section is mirrored from the [libtw2](https://github.com/heinrich5991/libtw2) documentation and is dual-licensed under MIT or APACHE.
{{% /notice %}}

Technical documentation of Teeworlds file formats and network protocol

 * [Connection](connection)
 * [Datafile](datafile)
 * [Demo](demo)
 * [Huffman](huffman)
 * [Int](int)
 * [Map](map)
 * [Packet](packet)
 * [Quirks](quirks)
 * [Serverinfo extended](serverinfo_extended)
 * [Snapshot](snapshot)
 * [Teehistorian](teehistorian)
