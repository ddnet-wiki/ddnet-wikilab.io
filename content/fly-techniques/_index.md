---
title: "Fly Techniques"
weight: 30
chapter: true
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/ for the current version
{{% /notice %}}

# Fly Techniques

[Hammerfly](hammerfly)

{{% vid hammerfly-free %}}

[Hookfly](hookfly)

{{% vid hookfly-free %}}

[Rocketfly](rocketfly)

{{% vid grenadefly-free %}}

