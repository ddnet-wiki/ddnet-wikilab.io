---
title: "Rocketfly"
weight: 10
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/Rocketfly for the current version
{{% /notice %}}

### Introduction

Rocketfly is a hard fly technique for 2 tees, and is pretty much only used when one of them is deep frozen (aka deep).
It requires the grenade launcher and is a lot easier with strong hook.
Doing the rocketfly consistently mostly comes down to feel.

### Easier Variant: Using a Wall

With a wall at your disposal you can use a much easier variant of the rocketfly:

- place the deep tee next to the wall
- jump on top of it
- aim down, a bit away from the wall
- repeating cycle:
  - hook
  - fire

{{% vid grenadefly-wall %}}

### General Principle

To initiate rocketfly, jump over the frozen tee and hook him below you.

To rocketfly to the right:

- try to stay on top of the other tee and slightly on the left side of it
- shoot the grenade whenever the other tee has just bumped into you (if you have weak hook, you might not bump properly)
- continuously hook the other tee, rehook only directly before firing
- aim downwards and a bit to the left
- repeating cycle:
  - shot a grenade
  - adjust your position by moving a bit to the right yourself

To rocketfly to the left, do the same movements but mirrored.
Each shot of the grenade launcher will propell you upwards and yourself as well as the other tee sideways.

{{% vid grenadefly-free %}}

#### Adjusting The Angle

How far you go upwards and to the side over time depends on the angle you shoot your rocket at.

When you aim further to the side, you will get launched stronger sideways.
Note that you also have to adjust your position more.

The closer you are to aiming straight down, the more height you will gain and the less you will have to adjust your position.

#### Losing Height While Rocketfly

During vertical flight it is hard not to gain height over time.
Once you need to lose height, stay above the other tee and hook it occasionally to drop slowly (same technique as when losing height during hammerfly).
To continue to rocketfly, hook the other tee towards you and start the cycle again.


### Flying Upwards

While the positioning stays the same (decide for one side of the tee to stay on), you now fire straight downwards and only slightly to the side.
Each shot of the grenade launcher will propell your tee upwards significantly and the frozen tee to the side.
The hook will mostly correct your position to the other tee, but you might have to manually adjust your position by a bit.

### Switching Direction

Whether you are flying horizontally or vertically, you might need to switch your flight direction for one reason or another.
To practice switching the direction mid-flight, first practice each direction individually and then try the switch.
Switch by changing your position relative to the other tee while you are hooking it towards you, and changing the angle of your grenade launcher.
