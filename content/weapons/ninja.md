---
title: "Ninja"
weight: 60
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/Ninja for the current version
{{% /notice %}}

Ninja is a different to the other weapons by being more like a power-up.

Upon collection ninja, you can dash around by firing it.
You can't switch to any other weapon while you have ninja equipped. It will run out after 15 seconds or by touching shields.
Using the dash allows quick movement and flight.

| Quickinfo  |       |
| ---------- | ----- |
| Gameskin   | ![Ninja](/images/ninja.png) |
| Crosshair  | ![Ninja crosshair](/images/ninja-crosshair.png) |
| Fire delay | 800ms |

### Dashing

Once Ninja is picked up, you can dash in any direction with a relatively long cooldown of 800ms.  

Properties:

- dash distance: <span title="14#02">14,06</span> tiles (450 base units)
- dash time: 180ms (9 ticks)
- velocity of <span title="1#18">1,56</span> tiles per tick (50 base units per tick)
- during the dash all other forces like gravity and active hooks are ignored
- ninja passes through other tees, laser doors and stopper tiles.

Whenever you hit a wall during your dash, the direction of the dash will remain unchanged, resulting in you sliding along it.  
[Video of sliding along a wall with ninja]  
After the dash any previous momentum will be redirected in the same direction of the dash.  
[Video of redirecting momentum]  

The moment you no longer have ninja, your current dash will stop immediately.

### Dash through teleporter

It is possible to pass a 1-tile broad teleporter by dashing horizontally or vertically.
Tiles that can be dashed through:

- teleporter/checkpoints
- freeze
- startline/finishline
- switches
- tiles that give or remove jetpack/endless hook/infinite jumps and similar tiles

This trick is very precise and works because dashing tees move 50 units per tick, while teleporter are only 32 units wide.
Being one tile away from the wall works for dashing through.

{{% vid ninja-dash %}}

### Advanced Properties
- tees that you pass through during a dash will get pushed upwards and to the side
- multiple tees can get on the exact same position by dashing, to get it to work easily, use corners.

