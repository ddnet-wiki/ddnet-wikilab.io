---
title: "Hammer"
weight: 10
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/Hammer for the current version
{{% /notice %}}

The Hammer is a melee, semi-automatic weapon.  
It is one of the 2 always accessible weapons, and it is probably the one you will be using the most.  
Its main purpose is to unfreeze other tees and manipulate their movement.

| Quickinfo  |       |
| ---------- | ----- |
| Gameskin   | ![Hammer](/images/hammer.png) |
| Crosshair  | ![Hammer crosshair](/images/hammer-crosshair.png) |
| Fire delay | 120ms (320ms when hitting another tee) |

### General Information
As the only melee weapon, the hammer has a relatively short range.  
Any tees hit by its generous hitbox will

- get unfrozen
- get pushed away from the hammering tee

The range of the hammer allows you to just barely hit tees behind a 1 tile thick wall.  

<table>
	<tr>
		<th></th>
		<th>Left</th>
		<th>Not moving</th>
		<th>Right</th>
	</tr>
	<tr>
		<td>Without jump</td>
		<td>{{% vid hammer-left-nojump %}}</td>
		<td>{{% vid hammer-nomovement-nojump %}}</td>
		<td>{{% vid hammer-right-nojump %}}</td>
	</tr>
	<tr>
		<td>With jump</td>
		<td>{{% vid hammer-left-jump %}}</td>
		<td>{{% vid hammer-nomovement-jump %}}</td>
		<td>{{% vid hammer-right-jump %}}</td>
	</tr>
</table>


| Result            | `Left` | `Not moving` | `Right` | Jump height |
| ----------------- | ------ | ------------ | ------- | ----------- |
| `Without jumping` | 2#11   | 1#22         | 4#12    | 2#00        |
| `Jumping`         | 2#23   | 1#28         | 5#03    | 5#22        |

### Unfreezing
When you hit another tee, it will immediately unfreeze them.  

#### 1-Tick-Unfreeze
If the tee you are hammering is currently in a freeze tile, they will immediately become frozen again.
However, just like when hit by the Laser Rifle, the tee will unfreeze for a brief moment in which they can:

- jump
- move (only very little)
- fire any weapon

With any weapon, the briefly unfrozen tee can hold down the fire button to fire instantly upon being unfrozen by the hammer.  
Using this you can create chain reactions using the 1-tick-unfreeze.

### Movement Using Hammering

On top of being unfrozen, the hit tee will get a good amount of speed.
While there are a lot of tricks with the hammer alone, combining it with other utilities allows for many more tricks.

#### Hammerrun

Hammerrun combines running speed with the horizontal speed from the hammer.

- the tee in the back initiates it by walking against the tee in the front
- the one in the front can then also begin walking in the same direction
- now the tee in the back can hammer the one in front to give it speed

{{% vid hammerrun %}}

#### Hammerjump

Hammerjump combines jumping with the vertical speed from the hammer.
It is essential that the hammer hit happens after that jump.

{{% vid hammerjump %}}

### Advanced Behaviour
- although the Hammer is a semi-automatic weapon, it can be held down to shoot directly on unfreeze
- counterintuitively, the hammer will never cause downwards momentum, even when the hammering tee is on top of the other tee. It will only cause sideways and upwards momentum
