---
title: "Weapons"
date: 2019-10-24T00:25:06+02:00
weight: 10
chapter: true
---

### Game physics using

# Weapons

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/ for the current version
{{% /notice %}}

#### Always equipped weapons

[![Hammer](/images/hammer.png)](hammer)
[![Pistol](/images/pistol.png)](pistol)

#### Collectable weapons

[![Shotgun](/images/shotgun.png)](shotgun)
[![Grenade](/images/grenade.png)](grenade)
[![Laser](/images/laser.png)](laser)

#### Power-ups

[![Ninja](/images/ninja.png)](ninja)
