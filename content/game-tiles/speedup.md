---
title: "Speedup Layer"
explain: true
weight: 60
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/Tiles for the current version
{{% /notice %}}

#### Tile representations of the 'speedup' layer in the editor.

Contains the speedup tile.
Necessary due to its unique properties.

{{% explain file="static/explain/speedup.svg" %}}
