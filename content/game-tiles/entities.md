---
title: "Game Layer"
explain: true
weight: 20
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/Tiles for the current version
{{% /notice %}}

#### Tile representations of the 'game' layer in the editor.

Primary tile layer of maps.
Includes most of the relevant tiles.

{{% explain file="static/explain/entities.svg" %}}
