---
title: "Tune Layer"
explain: true
weight: 70
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/Tiles for the current version
{{% /notice %}}

#### Tile representations of the 'tune' layer in the editor.

Contains the tune zone tile.
Necessary due to its unique properties. 

{{% explain file="static/explain/tune.svg" %}}
