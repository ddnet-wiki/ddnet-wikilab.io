---
title: "Front Layer"
explain: true
weight: 30
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/Tiles for the current version
{{% /notice %}}

#### Tile representations of the 'front' layer in the editor.

Secondary tile layer of maps. Excludes collision blocks and adds tiles that are only relevant when layered over other tiles.

{{% explain file="static/explain/front.svg" %}}
