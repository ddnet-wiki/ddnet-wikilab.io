---
title: "Teleporter Layer"
explain: true
weight: 50
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/Tiles for the current version
{{% /notice %}}

#### Tile representations of the 'tele' layer in the editor.

Teleporter tile layer of maps.
Includes all relevant tiles for teleporters.

{{% explain file="static/explain/tele.svg" %}}
