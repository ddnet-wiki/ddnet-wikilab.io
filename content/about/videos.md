---
title: "Creating videos"
weight: 20
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/How_to_contribute for the current version of this page
{{% /notice %}}

## Step 1: Getting a Map

### Variant 1: Using an existing map

To use maps from videos in the wiki:

1. rightclick the video and click 'View Video', now you can find the name of the video in the url
2. go [here](https://gitlab.com/ddnet-wiki/ddnet-wiki.gitlab.io/-/tree/master/static/videos) and download the correct demo
3. open the demo in your client (you need to move the demo into your `data/maps` folder)
4. while you have the demo open, go into the editor click `File` and select `Load Current Map`

Now you can edit and use the map for the demo. 

### Variant 2: Creating a new map

1. create a new map in the editor
2. set the background color to `#5E84AE`
    1. click on the `quads` layer
    2. rightclick the corners of the rectangle 
    3. insert the color value `#5E84AE` into the third to last field each each
3. the main focus should be that it is comprehensible
4. keep the style simple and similar to the other videos
    - use simple textures (preferably `grass_main`, combined with `generic_unhookable` and `font_teeworlds`)
    - make it loopable, you can get creative with that :)
    - insert a scale if suited

## Step 2.1: Recording the demo

1. turn demo recording on
    - i suggest `Settings -> General -> Automatically record demos`
2. load the map on a server
    - start your own ddnet server or use the [Trashmap](https://trashmap.timakro.de/) hosting service
3. join it with your client, try the trick until you are satisfied with the result, keep loopability in mind

### Recording Settings


Set consistent skin:

* hammer: brownbear
* pistol: coala
* shotgun: cammo
* grenade: redstripe
* laser: bluestripe
* hook: default
* jump: bluekitty

## Step 2.2: Cutting the demo

1. in your client, go to `Demos -> auto/`
2. open the demo you recorded
    - tip: click on `Date` to sort by date
3. press `Escape` for demo functions
4. use the 2 buttons on the left of the camera icons to select a starting and end point
5. use the camera icon to save the shortened demo

Tip: you can repeat these steps multiple times to be able to cut the demo more precisely

## Step 3: Converting the demo to a video

### Getting the suited client
For this step, you require the ddnet client with the [Demo to Video function](https://github.com/ddnet/ddnet/pull/1928).
On linux, you have to build ddnet yourself with the build argument `DVIDEORECORDER=ON`, also `DAUTOUPDATE=OFF` so that the client won't update and thereby remove the converting functionality.
To do so, follow the building instructions on the [ddnet github](https://github.com/ddnet/ddnet) and do `cmake -DVIDEORECORDER=ON -DAUTOUPDATE=OFF ..` instead of `cmake ..`.
This is not yet possible on windows.

Note: If the demo to video converter doesn't work for you, create a merge request with the demo and someone will take care of the video.

### Converting the demo


Once you have the client, converting a demo is as simple as:

- open the client (with the converting functionality)
- set zoom to 10: `cl_default_zoom 10`
- go to the `Demos` tab
- select the correct demo
- click `Render` at the bottom

The client will now render the video. You can find it in your teeworlds/ddnet folder in `data/videos`

## Step 4: Cropping the video

Now that we have our video, we will now want to crop it down, so that only the relevant area of it is shown.
For this exact process we have created the `crop.sh` script.
You can find it in `/scripts/crop.sh`.

To use it:

1. first **edit the `crop.sh` script to set the `PIXEL_PER_TILE` value correctly!**
2. use it as a commandline tool:
    - the first argument is the input video file
    - the second argument is the output video file
    - the third to sixth arguments are each, how many tiles upwards/downwards/to the left/to the right should be seen in the video
3. try different settings to get the best result
4. Remember to **include the exact command you used in the commit message, as well as your `PIXEL_PER_TILE` value**!

## Step 5: Creating a merge request for the repository

You don't have to be familiar with git to create a merge request:

1. go to the [folder view](https://gitlab.com/-/ide/project/ddnet-wiki/ddnet-wiki.gitlab.io/tree/master/-/static/videos/)
2. click on the drop-down menu next to the `videos` folder
3. click `Upload file`
4. upload all relevant files this way, for each video include the .demo
5. click on the blue button `Commit...`
6. write into the 'Commit Message' what you are changing (e.g. which video you are adding)
7. insert the crop command(s) you used from step 4!
8. 'Create a new branch' should be selected by default, if not, tick it
9. press the gren button 'Commit'

## Step 6: Insert videos in pages

See [About](..) for how to edit pages.
Adding {{\% vid vid-name %}} to markdown pages includes `/video/vid-name.mp4` in the html page.
