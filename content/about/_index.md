---
title: "About"
weight: 1000
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/How_to_contribute for the current version of this page
{{% /notice %}}

## Contributing

On each page you can click on 
<a class="github-link" title='Edit this page' href="#" target="blank" onclick="return false">
    <i class="fas fa-code-branch"></i>
    <span>Edit this page</span>
</a>
in the top right corner to open the corresponding file in Gitlab.

You can then click on "Edit" to open the [Gitlab Web Editor][1] to create merge request.

[1]: https://docs.gitlab.com/ee/user/project/repository/web_editor.html

### What to contribute?

We encourage every kind of contribution.
Here are a few examples what you could do:

- you can always fix problems, correct grammar, make sure wording is accurate, etc. (See [Editing Pages](editing)
- expand an article by covering another topic
- create videos for articles ([How to create videos](videos))
- write a new article. [Topics](https://gitlab.com/ddnet-wiki/ddnet-wiki.gitlab.io#topics) includes a variety of topics yet to cover

### See also:

- [Editing pages](editing)
- [Creating videos](videos)
- [Modifying game tile explanations](game-tiles)
