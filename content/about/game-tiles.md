---
title: "Modify Game Tile Explanation"
weight: 30
---

{{% notice warning %}}
This wiki is deprecated. Visit https://github.com/ddnet/ddnet-web/tree/master/explain-layers#readme for the current version of this page
{{% /notice %}}

* Edit explanation [`/static/explain/tiles.csv`](/explain/tiles.csv) with libreoffice (or your text editor of your choice).
* Edit layout in
  * `layouts/partials/custom-header.html`: including dependencies
  * `layouts/shortcodes/explain.html`: layout for embedding
  * `static/explain/tiles.csv`: descriptions
  * `static/explain/template.svg`: layout for hovering
* Regenerate all svg files with by executing `scripts/regenerate.sh` in `static/explain/`

