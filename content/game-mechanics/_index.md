---
title: "Game Physics"
weight: 20
chapter: true
---

### Further

# Game physics

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/ for the current version
{{% /notice %}}

### Tools for movement:

### [Jump](jump), [Hook](hook)

### Technical articles:

### [World](world), [Movement](movement)
