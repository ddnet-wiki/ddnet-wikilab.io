---
title: "World"
weight: 30
---

{{% notice warning %}}
This wiki is deprecated. Visit https://wiki.ddnet.org/wiki/World for the current version
{{% /notice %}}

The game world is a square grid of tiles, with a width and height of 32 units.
Tees have a dimension of 29x29 units.

